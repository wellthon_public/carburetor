package main

import (
	"log"
	"os"
	"testing"

	"gitlab.com/wellthon_public/carburetor/fileutils"
	"gitlab.com/wellthon_public/carburetor/scripts"
)

func TestFFMPEGAudioSplit(t *testing.T) {
	audioName := "ourself.aac"
	if !fileutils.Exists(audioName) {
		log.Printf("audio file %s does not exist\n", audioName)
		return
	}

	testDir := "./test-aud/"
	if !fileutils.Exists(testDir) {
		fileutils.CreateDirectory(testDir)
	}
	err := scripts.SplitFile(audioName, "audio", testDir)
	if err != nil {
		t.Errorf("FFMPEG failed to split audio file: %v", err)
	}
	os.RemoveAll(testDir)
}

func TestFFMPEGVideoSplit(t *testing.T) {
	vidName := "out.mp4"
	if !fileutils.Exists(vidName) {
		log.Printf("audio file %s does not exist\n", vidName)
		return
	}

	testDir := "./test-vid/"
	if !fileutils.Exists(testDir) {
		fileutils.CreateDirectory(testDir)
	}
	err := scripts.SplitFile(vidName, "video", testDir)
	if err != nil {
		t.Errorf("FFMPEG failed to split video file: %v", err)
	}
	os.RemoveAll(testDir)
}
