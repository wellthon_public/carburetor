package main

import (
	"fmt"
	"path/filepath"
	"testing"

	"gitlab.com/wellthon_public/carburetor/constants"
)

func TestAudioChunkMatch(t *testing.T) {
	vidFName := fmt.Sprintf("%s1080p_012.ts", constants.VideoChunksDIR)
	audioFName := "ourself.aac"
	audioChunk := getAudioChunkMatch(vidFName, audioFName)
	expected := "ourself012.ts"
	expected2 := "ourself012.aac"
	actual := filepath.Base(audioChunk)

	if actual != expected && actual != expected2 {
		t.Errorf("Expected the audio chunk match of %s to be %s or %s but instead got %s", vidFName, expected, expected2, actual)
	}
}
