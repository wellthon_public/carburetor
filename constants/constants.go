package constants

const (
	EdgeServerURL   = "https://carburetor-video-files.sfo2.digitaloceanspaces.com/"
	VideoChunksDIR  = "video-chunks/"
	AudioChunksDIR  = "audio-chunks/"
	MixedChunksDIR  = "mixed-chunks/"
	MixerScriptName = "ffmpeg-vid-aud-mix.sh"
)
