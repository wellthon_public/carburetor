let spacesServer = ''

const PORT = 3001
const serverName = `http://localhost:${PORT}`
const manifestNames = ['playlist.m3u8', '240p.m3u8', '360p.m3u8', '480p.m3u8', '720p.m3u8', '1080p.m3u8']
let fileContent = {}
let activeFile = ''

function loadFile(fName) {
    return fetch(`${spacesServer}/${fName}`)
        .then(response => {
            if (response.ok) return response.text()
            else {
                return new Promise(resolve => resolve(`${response.status}`))
            }
        })
        .catch(err => console.log("Error found", err))
}

function getAllFileContent(fileNames) {
    promises = []
    fileNames.forEach(fName => {
        promises.push(loadFile(fName))
    });

    Promise.allSettled(promises).then(contents => {
        contents.forEach((result, i) => {
            if (result.status == "fulfilled") {
                fileContent[manifestNames[i]] = result.value
            }
        });

        return new Promise(resolve => {
            resolve()
        })
    }).then(() => {
        fileNames.forEach(name => addContentToDOM(name))
    })
}

function addContentToDOM(fileName) {
    let fileContentArea = document.querySelector("#file-content")
    fileContentArea.value = fileContent[fileName];
    activeFile = fileName;
}

function clearText(el) {
    el.innerHTML = ''
}

function handleFileUpload() {
    let data = document.querySelector("#file-content").value;
    let content = { 'name': activeFile, 'content': data }
    fetch(`${serverName}/upload`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(content),
    })
        .then(response => response.json())
        .then(data => {
            console.log(data)
        })
        .catch(err => {
            console.error(err)
        })
}

document.addEventListener('DOMContentLoaded', () => {
    let fileContentArea = document.querySelector("#file-content")
    let fileTags = document.getElementsByClassName("file-link")
    let serverInputEl = document.querySelector("#server-name")

    fetch(`${serverName}/serverurl`)
        .then(response => {
            if (response.ok) return response.json()
            else {
                return new Promise(resolve => resolve(`${response.status}`))
            }
        })
        .then(data => {
            spacesServer = data.serverurl
            serverInputEl.value = spacesServer;
            getAllFileContent(manifestNames)
        })
        .catch(err => console.log("Error found", err))


    for (let i = 0; i < fileTags.length; i++) {
        fileTags[i].addEventListener('click', function () {
            clearText(fileContentArea)
            addContentToDOM(fileTags[i].text)
        })
    }

    document.querySelector(".update-btn").addEventListener('click', () => {
        handleFileUpload()
    })

    serverInputEl.addEventListener("keydown", (e) => {
        if (e.keyCode === 13) {  //checks whether the pressed key is "Enter"
            spacesServer = serverInputEl.value
            getAllFileContent(manifestNames)
        }
    })
})

