const aws = require('aws-sdk');
const express = require('express');
const bodyParser = require('body-parser');
const { argv } = require('process');
const yargs = require('yargs');
const { Endpoint } = require('aws-sdk');

const app = express();

let ServerURL = "";
let ServerEndpoint = "";

yargs
  .options({
    'surl': {
      alias: 'serverurl',
      default: '',
      describe: 'Your S3-compatible server url',
      type: 'string'
    },
    'ep': {
      alias: 'endpoint',
      demandOption: true,
      default: '',
      describe: 'Your server endpoint',
      type: 'string'
    },
  })
  .usage("Usage: node $0 -surl <server_url> -ep <server_endpoint>")
  .demandOption(["surl", "ep"])
  .help()
  .argv

// Set S3 endpoint to DigitalOcean Spaces
let spacesEndpoint = null
let s3 = null

app.use(express.static("public"))
app.use(bodyParser.json())

app.get('/', function (request, response) {
  response.sendFile(__dirname + '/public/index.html');
});

app.get('/serverurl', (_, res) => {
  res.status(200).json({ 'serverurl': ServerURL })
})

// receives the content to be uploaded
app.post('/upload', (req, res) => {
  const body = req.body

  let params = {
    Body: body.content,
    Bucket: 'carburetor-video-files',
    Key: body.name,
    ACL: 'public-read'
  }

  s3.putObject(params, (err, data) => {
    if (err) {
      console.log(err, err.stack)
    }
    else console.log(data)
  })
})

if (yargs.argv.endpoint != "" && yargs.argv.serverurl != "") {
  ServerURL = yargs.argv.serverurl;
  ServerEndpoint = yargs.argv.endpoint;

  spacesEndpoint = new aws.Endpoint(ServerEndpoint);
  s3 = new aws.S3({
    endpoint: ServerEndpoint
  });

  app.listen(3001, function () {
    console.log("server listening on port 3001")
  });
}