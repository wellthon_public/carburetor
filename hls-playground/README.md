
## HLS Playground

A web client to manipulate your HLS manifest files living on any S3-compatible server.

### Screenshot
![Screenshot](hls-playground.png)

### How to Run
1. Add Access Keys to Credentials File
	- create a credentials file to place your servers access key and secret access key. The file will be located at `~/.aws/credentials` on Mac and Linux , or `C:\Users\USERNAME\.aws\credentials`on Windows.

		You can use the command `sudo mkdir .aws && touch .aws/credentials` to create the file. Then add your keys.

		```
		[default]
		aws_access_key_id=your_access_key
		aws_secret_access_key=your_secret_key
		```
2.  Run `npm install` to download dependencies.
3. Add your server url and endpoint and start up the server by running `node server.js --serverurl=<your_server_url> --endpoint=<your_server_endpoint>` .
What is the difference between server url and endpoint url? For [Digitalocean spaces](https://www.digitalocean.com/docs/spaces/), if my server url is `https://carburetor-video-files.sfo2.digitaloceanspaces.com` i.e `https://<BUCKET_NAME>.<REGION>.digitaloceanspaces.com` then my endpoint is `sfo2.digitaloceanspaces.com`.
Refer to your server providers documentation for more info.

For help, run `node server.js --help`
