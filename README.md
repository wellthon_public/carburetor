# Carburetor

## Audio & video mixer
Carburetor is a live audio and video mixer. It is able to take another audio input stream and mix it with the video stream.

## Experimental Architecture
![architecture image](architecture.svg)

### Steps
1. The client requests video or audio data through http.
2. The mixer server receives this request and sends a request to the server containing all the video and audio data.
3. The server then responds with the data.
4. The mixer server mixes the audio and video then sends out the mixed chunk.

For HLS files, the server serves segments of the data with a default length of max 10 seconds. HLS files also contain a metadata file called .m3u8 which is served first. It contains information on where to get the different stream resolutions.
For MPEG files, it also contains its own manifest file and segments of 2-4 seconds in length. 
They both contain duplicate segments of varying qualities to support adaptive bitrate streaming.
