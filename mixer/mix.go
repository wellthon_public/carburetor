package mixer

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"

	"gitlab.com/wellthon_public/carburetor/constants"
)

func checkError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}

func Mix(videoFName string, audioFName string) string {
	fmt.Println("mixing")
	mixedFName := constants.MixedChunksDIR + filepath.Base(videoFName)
	ffmpegCmdStrFormat := "ffmpeg -i %s -i %s -c:v copy -c:a aac -filter_complex amix -map 0:v -map 0:a -map 1:a %s"
	mixerCmd := fmt.Sprintf(ffmpegCmdStrFormat, videoFName, audioFName, mixedFName)

	fmt.Println(mixerCmd)
	f, err := os.Create(constants.MixerScriptName)
	checkError(err)
	defer f.Close()

	f.WriteString("#!/bin/bash\n")
	f.WriteString(mixerCmd)
	err = exec.Command("/bin/bash", constants.MixerScriptName).Run()
	log.Printf("Command finished with error: %v", err)
	checkError(err)
	fmt.Println("mixed file path -> ", mixedFName)
	return mixedFName
}
