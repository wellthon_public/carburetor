package fileutils

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
)

// Writes the content of body to fName
// The file is created if it does not exist
func WriteToFile(fName string, body []byte) {
	err := ioutil.WriteFile(fName, body, 0744)
	if err != nil {
		log.Fatal(err)
	}
}

// appends body to the end of the file
// The file is created if it does not exist
func AppendToFile(fName string, body []byte) {
	f, err := os.OpenFile(fName,
		os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0744)
	if err != nil {
		log.Println(err)
	}
	defer f.Close()
	if _, err := f.Write(body); err != nil {
		log.Println(err)
	}
}

// clears the content of a file
func Truncate(filename string) error {
	if !Exists(filename) {
		log.Printf("Truncation Error: The file %s does not exist\n", filename)
		return nil
	}

	f, err := os.OpenFile(filename, os.O_TRUNC|os.O_CREATE, 0744)
	if err != nil {
		return fmt.Errorf("could not open file %q for truncation: %v", filename, err)
	}
	if err = f.Close(); err != nil {
		return fmt.Errorf("could not close file handler for %q after truncation: %v", filename, err)
	}
	return nil
}

// Exists checks if a file or directory exists and is not a directory before we
func Exists(filename string) bool {
	if _, err := os.Stat(filename); os.IsNotExist(err) {
		return false
	}
	return true
}

// returns a slice of all lines of the file
func GetFileContent(fName string) []string {
	if !Exists(fName) {
		log.Println("File does not exist")
		return []string{}
	}
	var content []string
	file, err := os.Open(fName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		content = append(content, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return content
}

// prints the content of a file line by line
func PrintFile(fName string) {
	if !Exists(fName) {
		return
	}
	file, err := os.Open(fName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func CreateDirectory(dirName string) {
	err := os.Mkdir(dirName, 0744)
	if err != nil {
		log.Fatal(err)
	}
}

// removes all files in a directory
func ClearDirectory(dirName string) {
	if !Exists(dirName) {
		log.Printf("The directory %s does not exist\n", dirName)
		return
	}

	dir, err := ioutil.ReadDir(dirName)
	if err != nil {
		log.Fatal(err)
	}

	for _, file := range dir {
		fullName := path.Join(dirName, file.Name())
		if err := os.RemoveAll(fullName); err != nil {
			log.Fatal(err)
		}
	}
}
