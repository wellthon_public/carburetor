package manifest

import (
	"testing"
)

func TestExtINFSize(t *testing.T) {
	filePath := "../stubs/1080p.m3u8"
	manifest := GetEXTINFS(filePath)
	expected := Manifest{
		EXTINF: []float64{
			5.005000,
			3.336667,
			5.005000,
			3.336667,
			3.336667,
			5.005000,
		},
	}

	actualSize := len(manifest.EXTINF)
	expectedSize := len(expected.EXTINF)
	if actualSize != expectedSize {
		t.Errorf("expected size of Manifest.EXTINF to be %v. got %v", expectedSize, actualSize)
	}
}

func TestExtINFS(t *testing.T) {
	filePath := "../stubs/1080p.m3u8"
	manifest := GetEXTINFS(filePath)
	expected := Manifest{
		EXTINF: []float64{
			5.005000,
			3.336667,
			5.005000,
			3.336667,
			3.336667,
			5.005000,
		},
	}

	for i, v := range manifest.EXTINF {
		if v != expected.EXTINF[i] {
			t.Errorf("expected %v to be %v", manifest.EXTINF, expected.EXTINF[i])
		}
	}
}
