package manifest

import (
	"bufio"
	"log"
	"os"
	"regexp"
	"strconv"
)

type Manifest struct {
	EXTINF []float64
}

func getFileContent(fName string) []string {
	var content []string
	file, err := os.Open(fName)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		content = append(content, scanner.Text())
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
	return content
}

func GetEXTINFS(filePath string) Manifest {
	manifest := Manifest{}
	content := getFileContent(filePath)
	for _, line := range content {
		re := regexp.MustCompile(`#EXTINF:(.+),`)
		match := re.FindSubmatch([]byte(line))
		if len(match) > 1 {
			dur, err := strconv.ParseFloat(string(match[1]), 64)
			if err != nil {
				log.Fatal(err)
			}
			manifest.EXTINF = append(manifest.EXTINF, dur)
		}
	}
	return manifest
}
