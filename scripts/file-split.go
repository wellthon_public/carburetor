package scripts

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strings"

	"gitlab.com/wellthon_public/carburetor/constants"
	"gitlab.com/wellthon_public/carburetor/fileutils"
	"gitlab.com/wellthon_public/carburetor/httpUtils"
	"gitlab.com/wellthon_public/carburetor/manifest"
)

const TimeRangeFile = "./time-ranges.txt"

type TimeStamp struct {
	hour int
	min  int
	sec  float64
}

// Durations is assumed to be a slice of seconds
func getContiguoisTimeStamps(durations []float64) []TimeStamp {
	hr, min, sec := 0, 0, 0.0
	splitAudioRanges := []TimeStamp{}
	for _, dur := range durations {
		newSec := sec + dur
		if int(newSec) >= 60 {
			min += int((int(newSec) / 60))
			newSec = newSec - 60.0
		}
		if min >= 60 {
			hr += 1
			min = 0
		}
		sec = newSec
		splitAudioRanges = append(splitAudioRanges, TimeStamp{hr, min, sec})
	}
	return splitAudioRanges
}

func formatToHHTTSS(tStamp TimeStamp) string {
	return fmt.Sprintf("%02d:%02d:%f", tStamp.hour, tStamp.min, tStamp.sec)
}

// the time ranges generated is used to split the audio or video file
func generateTimeRangesFromEXTINF() []TimeStamp {
	// get the EXTINF value for all video chunks
	// this data will be used to divide static audio files
	var manifestData manifest.Manifest

	body := httpUtils.GetBodyFromURL(constants.EdgeServerURL + "1080p.m3u8")
	fileutils.WriteToFile(constants.VideoChunksDIR+"1080p.m3u8", body)

	manifestData = manifest.GetEXTINFS(constants.VideoChunksDIR + "1080p.m3u8")

	timeRanges := []TimeStamp{{hour: 0, min: 0, sec: 0}}
	timeRanges = append(timeRanges, getContiguoisTimeStamps(manifestData.EXTINF)...)

	return timeRanges
}

func populateTimeRangesFile(fName string, chunkTimeStamps []TimeStamp) {
	extension := filepath.Ext(fName)
	name := strings.TrimSuffix(fName, extension)
	fmt.Println(extension, name)

	// filename format: name000.extension
	curFileName := fmt.Sprintf("%s%03d%s", name, 0, extension)
	fileutils.Truncate(TimeRangeFile)
	for i := 1; i < len(chunkTimeStamps); i += 1 {
		// duration format: 00:00:00 00:00:30 filename
		dur := fmt.Sprintf("%s %s %s\n", formatToHHTTSS(chunkTimeStamps[i-1]),
			formatToHHTTSS(chunkTimeStamps[i]), curFileName)
		fileutils.AppendToFile(TimeRangeFile, []byte(dur))
		curFileName = fmt.Sprintf("%s%03d%s", name, i, extension)
	}
}

func SplitFile(fName, fType, storeDir string) error {
	if fType != "audio" && fType != "video" {
		log.Fatal("File type must be either 'audio' or 'video'")
	}

	chunkTimeStamps := generateTimeRangesFromEXTINF()
	populateTimeRangesFile(fName, chunkTimeStamps)

	fileutils.ClearDirectory(storeDir)
	for _, line := range fileutils.GetFileContent(TimeRangeFile) {
		if err := fileutils.Truncate(constants.MixerScriptName); err != nil {
			log.Fatal(err)
		}
		f, err := os.OpenFile(constants.MixerScriptName, os.O_WRONLY, 0644)
		defer f.Close()
		if err != nil {
			log.Fatal(err)
		}

		cmdFormat := ""
		if fType == "audio" {
			cmdFormat = "ffmpeg -i %s -c:a copy -ss %s -to %s %s"
		} else {
			cmdFormat = "ffmpeg -i %s -c:a copy -c:v copy -ss %s -to %s %s"
		}
		values := strings.Split(line, " ")
		from, to, chunkName := values[0], values[1], values[2]
		mixerCmd := fmt.Sprintf(cmdFormat, fName, from, to, storeDir+chunkName)

		f.WriteString("#!/bin/bash\n")
		f.WriteString(mixerCmd)
		fmt.Printf("splitting from %s to %s -> %s\n", values[0], values[1], values[2])
		fmt.Println(mixerCmd)
		cmd := exec.Command("/bin/bash", constants.MixerScriptName)
		stdoutStderr, err := cmd.CombinedOutput()
		if err != nil {
			log.Println("FFMPEG error -> ")
			log.Fatal(fmt.Sprint(err) + ": " + string(stdoutStderr))
			return err
		}
		log.Printf("Command finished with error: %v\n\n", err)
	}
	return nil
}
