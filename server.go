package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gofiber/fiber"
	"github.com/gofiber/fiber/middleware/cors"
	"gitlab.com/wellthon_public/carburetor/constants"
	"gitlab.com/wellthon_public/carburetor/fileutils"
	"gitlab.com/wellthon_public/carburetor/httpUtils"
	"gitlab.com/wellthon_public/carburetor/mixer"
	"gitlab.com/wellthon_public/carburetor/scripts"
)

func getAudioChunkMatch(videoFileName, audio string) string {
	audioPrefix := audio[:len(audio)-len(filepath.Ext(audio))]

	// it is assumed that video and audio chunks follow the same number pattern,
	// i.e if a video chunk is named 1080p_023.ts then the audio chunk
	// must be named songname023.extension
	suffix := len(videoFileName) - 6
	chunkNum := videoFileName[suffix : suffix+3]

	// ignore .m3u8 files
	if _, err := strconv.Atoi(chunkNum); err == nil {
		audioFile := fmt.Sprintf("%s%s%s.ts", constants.AudioChunksDIR, audioPrefix, chunkNum)
		audioFile2 := fmt.Sprintf("%s%s%s.aac", constants.AudioChunksDIR, audioPrefix, chunkNum)
		if fileutils.Exists(audioFile) {
			return audioFile
		} else if fileutils.Exists(audioFile2) {
			return audioFile2
		}
	}
	return ""
}

func defineFlags(remove, sendMixedChunk, splitAudio *bool) {
	flag.BoolVar(remove, "remove", false, "remove any mixed chunks before mixing again")

	sendMixedDesc := "For testing purposes. \n Send out mixed chunks to client?"
	flag.BoolVar(sendMixedChunk, "sendmixed", true, sendMixedDesc)

	flag.BoolVar(splitAudio, "splitaudio", false, "split audio file based on the duration of chunks defined in the playlist")

	flag.Usage = func() {
		fmt.Fprintf(flag.CommandLine.Output(), "Usage of %s:\n", os.Args[0])
		flag.PrintDefaults()
	}
	flag.Parse()
}

func main() {
	fmt.Println("The server address is https://bubble-wellthon.tk/playlist.m3u8")
	var remove, sendMixedChunk, splitAudio bool
	defineFlags(&remove, &sendMixedChunk, &splitAudio)

	if remove {
		dirName := "mixed-chunks"
		fileutils.ClearDirectory(dirName)
	}

	tracks := []string{"ourself.aac"}
	if splitAudio {
		fmt.Printf("splitting audio...\n")
		scripts.SplitFile(tracks[0], "audio", constants.AudioChunksDIR)
	}

	app := fiber.New()
	app.Use(cors.New())

	app.Get("/:fname", func(c *fiber.Ctx) error {
		path := c.Params("fname")

		// get the video chunk, audio chunk match and write it to a file
		videoChunkURL := fmt.Sprintf("%stest/%s", constants.EdgeServerURL, path)
		body := httpUtils.GetBodyFromURL(videoChunkURL)
		videoFilePath := constants.VideoChunksDIR + path
		audioFilePath := getAudioChunkMatch(videoFilePath, tracks[0])
		fileutils.WriteToFile(videoFilePath, body)

		chunkPath := ""
		// sendMixedChunk if a flag value used for testing purposes
		// i.e if you wanted to test the original video stream
		if sendMixedChunk {
			// get path of mixed file
			videoExtension, audioExtension := filepath.Ext(videoFilePath), filepath.Ext(audioFilePath)
			mixedFilePath := ""
			if videoExtension != "" && audioExtension != "" && videoExtension != ".m3u8" && audioExtension != ".m3u8" {
				mixedFilePath = mixer.Mix(videoFilePath, audioFilePath)
				fmt.Printf("mixed video chunk -> %s\n", mixedFilePath)
				chunkPath = mixedFilePath
			} else {
				chunkPath = videoFilePath
			}
		} else {
			chunkPath = videoFilePath
		}

		fmt.Printf("sending %s\n", chunkPath)
		err := c.SendFile(chunkPath)
		if err != nil {
			log.Fatal(err)
		}
		return c.SendStatus(200)
	})

	app.Listen(":3000")
}
